import React, { PropTypes } from 'react';
import Image from '../Images/Image';

const styles = {
  display: 'inline-block',
  transform: 'rotate(-7deg)',
  WebkitTransform: 'rotate(-7deg)'
};

const propTypes = {
  image: PropTypes.object
};

const ImageDragPreview = (props) => {
  styles.width = `${props.image.clientWidth || 100}px`;
  styles.height = `${props.image.clientHeight || 100}px`;

  return (
    <div style={styles}>
      <Image item={props.image.item} />
    </div>
    );
};

ImageDragPreview.propTypes = propTypes;

export default ImageDragPreview;
