import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import * as ListsActions from '../../../actions/lists';

import ImagesContainer from '../Images/ImageContainer';
import CustomDragLayer from './CustomDragLayer';

function mapStateToProps(state) {
  return {
    lists: state.lists.lists
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ListsActions, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
@DragDropContext(HTML5Backend)
export default class Board extends Component {
  static propTypes = {
    getLists: PropTypes.func.isRequired,
    moveImage: PropTypes.func.isRequired,
    lists: PropTypes.array.isRequired,
  }

  constructor(props) {
    super(props);
    this.moveImage = this.moveImage.bind(this);
    this.findList = this.findList.bind(this);
    this.scrollRight = this.scrollRight.bind(this);
    this.scrollLeft = this.scrollLeft.bind(this);
    this.stopScrolling = this.stopScrolling.bind(this);
    this.startScrolling = this.startScrolling.bind(this);
    this.state = {
      isScrolling: false
    };
  }

  componentWillMount() {
    this.props.getLists();
  }

  startScrolling(direction) {
    // if (!this.state.isScrolling) {
    switch (direction) {
      case 'toLeft':
        this.setState({
          isScrolling: true
        }, this.scrollLeft());
        break;
      case 'toRight':
        this.setState({
          isScrolling: true
        }, this.scrollRight());
        break;
      default:
        break;
    }
  // }
  }

  scrollRight() {
    function scroll() {
      document.getElementsByTagName('main')[0].scrollLeft += 10;
    }
    this.scrollInterval = setInterval(scroll, 10);
  }

  scrollLeft() {
    function scroll() {
      document.getElementsByTagName('main')[0].scrollLeft -= 10;
    }
    this.scrollInterval = setInterval(scroll, 10);
  }

  stopScrolling() {
    this.setState({
      isScrolling: false
    }, clearInterval(this.scrollInterval));
  }

  moveImage(lastX, lastY, nextX, nextY) {
    this.props.moveImage(lastX, lastY, nextX, nextY);
  }



  findList(id) {
    const {lists} = this.props;
    const list = lists.filter(l => l.id === id)[0];

    return {
      list,
      lastX: lists.indexOf(list)
    };
  }

  render() {
    const {lists} = this.props;
    let items = lists.map((item, i) => {


      if (i == 3) {
        return (<span>      </span>)
      } else {
        return (

          <ImagesContainer
          key={item.id}
          id={item.id}
          item={item}
          moveImage={this.moveImage}
          startScrolling={this.startScrolling}
          stopScrolling={this.stopScrolling}
          isScrolling={this.state.isScrolling}
          x={i}
          />
        )
      }
    });
    return (
      <div style={{
        height: '100%'
      }}>
        <CustomDragLayer snapToGrid={false} />
        {items}
      </div>
    )
  }
}
