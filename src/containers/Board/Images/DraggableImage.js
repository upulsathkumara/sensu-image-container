import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import { DragSource } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';

import Image from './Image';
import { Motion, spring } from 'react-motion';

function getStyles(isDragging) {
  return {
    display: isDragging ? 0.5 : 1
  };
}

var SHOWGLOBEL = false;



const imageSource = {
  beginDrag(props, monitor, component) {
    SHOWGLOBEL = !SHOWGLOBEL

    // dispatch to redux store that drag is started
    const {item, x, y} = props;
    const {id, title} = item;
    const {clientWidth, clientHeight} = findDOMNode(component);

    return {

      id,
      title,
      item,
      x,
      y,
      clientWidth,
      clientHeight
    };

  },
  endDrag(props, monitor) {

    document.getElementById(monitor.getItem().id).style.display = 'block';
    props.stopScrolling();

    SHOWGLOBEL = !SHOWGLOBEL
    console.log("workign fine when end drag");


  },
  isDragging(props, monitor) {

    const isDragging = props.item && props.item.id === monitor.getItem().id;
    return !isDragging;

  }
};

// options: 4rd param to DragSource https://gaearon.github.io/react-dnd/docs-drag-source.html
const OPTIONS = {
  arePropsEqual: function arePropsEqual(props, otherProps) {
    let isEqual = true;
    if (props.item.id === otherProps.item.id &&
      props.x === otherProps.x &&
      props.y === otherProps.y
    ) {
      isEqual = true;
    } else {
      isEqual = false;
    }
    return isEqual;
  }
};

function collectDragSource(connectDragSource, monitor) {

  return {
    connectDragSource: connectDragSource.dragSource(),
    connectDragPreview: connectDragSource.dragPreview(),
    isDragging: monitor.isDragging()
  };
}

@DragSource('image', imageSource, collectDragSource, OPTIONS)
export default class ImageComponent extends Component {
  static propTypes = {
    item: PropTypes.object,
    connectDragSource: PropTypes.func.isRequired,
    connectDragPreview: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number,
    stopScrolling: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  ;

  handleMouseDown = () => {

    SHOWGLOBEL = !SHOWGLOBEL;
  };

  handleTouchStart = (e) => {
    e.preventDefault();
    this.handleMouseDown();
  };
  componentDidMount() {
    this.props.connectDragPreview(getEmptyImage(), {
      captureDraggingState: true
    });
  }

  render() {
    const {isDragging, connectDragSource, item, x, y} = this.props;

    return connectDragSource(
      <div>


              <Motion style={{
        x: spring(SHOWGLOBEL ? 1 : 0, {
          stiffness: 1000,
          damping: 4,
          SHOWGLOBEL: !SHOWGLOBEL
        })
      }}>
                {({x}) =>
        // children is a callback which should accept the current value of
        // `style`
        <div  className="demo0">
                    <div   className="demo0-block"
        style={{
          WebkitTransform: `translate3d(${x}px, 2, 0)`,
          transform: `translate3d(${x}px, 0, 0)`,
        }} >         <Image style={getStyles(isDragging)} item={item} />
</div>
                  </div>
      }
              </Motion>
            </div>



    );
  }
}
