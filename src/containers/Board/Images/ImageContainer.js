import React, { Component, PropTypes } from 'react';
import { DropTarget, DragSource } from 'react-dnd';

import Images from './Images';

const listSource = {
  beginDrag(props) {
    return {
      id: props.id,
      x: props.x
    };
  },
  endDrag(props) {
    props.stopScrolling();
  }
};

const listTarget = {
  canDrop() {
    return false;
  },
  hover(props, monitor) {
    if (!props.isScrolling) {
      if (window.innerWidth - monitor.getClientOffset().x < 200) {
        props.startScrolling('toRight');
      } else if (monitor.getClientOffset().x < 200) {
        props.startScrolling('toLeft');
      }
    } else {
      if (window.innerWidth - monitor.getClientOffset().x > 200 &&
        monitor.getClientOffset().x > 200
      ) {
        props.stopScrolling();
      }
    }
    const {id: listId} = monitor.getItem();
    const {id: nextX} = props;
    if (listId !== nextX) {
      props.moveList(listId, props.x);
    }
  }
};

@DropTarget('list', listTarget, connectDragSource => ({
    connectDropTarget: connectDragSource.dropTarget(),
  }))
@DragSource('list', listSource, (connectDragSource, monitor) => ({
  connectDragSource: connectDragSource.dragSource(),
  isDragging: monitor.isDragging()
}))
export default class ImagesContainer extends Component {
  static propTypes = {

    item: PropTypes.object,
    x: PropTypes.number,
    moveImage: PropTypes.func.isRequired,
    isDragging: PropTypes.bool,
    startScrolling: PropTypes.func,
    stopScrolling: PropTypes.func,
    isScrolling: PropTypes.bool
  }

  render() {
    const {item, x, moveImage, isDragging} = this.props;
    const opacity = isDragging ? 0.5 : 1;

    return (

      <div className="desk" style={{
        opacity,
        width: "15%",
        padding: 0,
        margin: 0

      }}>
        <div className="desk-head">
        </div>

        <Images
      moveImage={moveImage}
      x={x}
      images={item.images}
      startScrolling={this.props.startScrolling}
      stopScrolling={this.props.stopScrolling}
      isScrolling={this.props.isScrolling}
      />
      </div>
      );
  }
}
