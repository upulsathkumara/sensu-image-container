import React, { PropTypes } from 'react';


const propTypes = {
  item: PropTypes.object.isRequired,
  style: PropTypes.object
};





const Image = (props) => {

  const {style, item} = props;

  if (item.id < item.total) {
    return (
      <div style={style} className="item" id={style ? item.id : null}>
          <img src={`/src/assets/images/${item.id}.jpg`} />
    </div>
      );
  } else {
    return (<div> </div>);
  }

};

Image.propTypes = propTypes;

export default Image;
