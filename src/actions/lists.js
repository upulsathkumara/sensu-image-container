
export const GET_LISTS_START = 'GET_LISTS_START';
export const GET_LISTS = 'GET_LISTS';
export const MOVE_IMAGE = 'MOVE_IMAGE';
export const MOVE_LIST = 'MOVE_LIST';
export const TOGGLE_DRAGGING = 'TOGGLE_DRAGGING';

export function getLists(quantity) {
  return dispatch => {
    dispatch({
      type: GET_LISTS_START,
      quantity
    });
    setTimeout(() => {
      const lists = [];
      let count = 0;

      for (let i = 0; i < 7; i++) {
        const images = [];
        // we can make this the length of the number of images
        const imageList = 17;

        if (i < 3) {
          for (let ic = 0; ic < imageList / 3; ic++) {
            images.push({
              id: count,
              total: imageList,
            });
            count = count + 1;
          }
        }
        lists.push({
          id: i,
          images
        });
      }
      dispatch({
        type: GET_LISTS,
        lists,
        isFetching: true
      });
    }, 1000);
    dispatch({
      type: GET_LISTS_START,
      isFetching: false
    });
  };
}

export function moveList(lastX, nextX) {
  return (dispatch) => {
    dispatch({
      type: MOVE_LIST,
      lastX,
      nextX
    });
  };
}

export function moveImage(lastX, lastY, nextX, nextY) {
  return (dispatch) => {
    dispatch({
      type: MOVE_IMAGE,
      lastX,
      lastY,
      nextX,
      nextY
    });
  };
}

export function toggleDragging(isDragging) {
  return (dispatch) => {
    dispatch({
      type: TOGGLE_DRAGGING,
      isDragging
    });
  };
}
