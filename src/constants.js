export const IMAGE_HEIGHT = 200; // height of a single card(excluding marginBottom/paddingBottom)
export const IMAGE_MARGIN = 10; // height of a marginBottom+paddingBottom
export const OFFSET_HEIGHT = 84; // height offset from the top of the page
